###########
# General #
###########

vo=gpu-hq
ad-lavc-downmix=yes
audio-channels=auto-safe # także opcja stereo

# Vulkan settings
gpu-api=vulkan 
vulkan-async-compute=yes
vulkan-async-transfer=yes
vulkan-queue-count=1
vd-lavc-dr=yes

hwdec=auto # enable best HW decoder; turn off for software decoding
reset-on-next-file=audio-delay,mute,pause,speed,sub-delay,video-aspect-override,video-pan-x,video-pan-y,video-rotate,video-zoom,volume
ytdl-format=bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4

######
# UI #
######

border=no # hide the window title bar
msg-color=yes # color log messages on terminal
term-osd-bar=yes # display a progress bar on the terminal
force-window=immediate
cursor-autohide=1000 # autohide the curser after 1s
# geometry=3840x2160 # force 4k resolution output from on Macs, rather than using MacOS upscaling
# fullscreen = yes # start in fullscreen mode by default


############
# Playback #
############

# deinterlace=no # global reset of deinterlacing to off


##############
# Colorspace #
##############

# see https://github.com/mpv-player/mpv/wiki/Video-output---shader-stage-diagram

# icc-contrast=1000 # hides warnings about ICC errors; disable for OLED displays
# target-prim=auto
# target-prim=bt.709
# target-prim=bt.2020 # target Rec.2020 (wide color gamut) for HDR TVs
#target-trc=auto
#gamma-auto
#vf=format=colorlevels=full:colormatrix=auto
#video-output-levels=full


##########
# Dither #
##########

#dither-depth=auto
#temporal-dither=yes


#############
# Debanding #
#############

#deband=yes # enabled by default 
#deband-iterations=4 # deband steps
#deband-threshold=20 # deband strength
#deband-range=16 # deband range
#deband-grain=0 # dynamic grain: set to "0" if using the static grain shader


#############
# Subtitles #
#############

#blend-subtitles=yes


#########################
# Motion Interpolation  #
#########################

#override-display-fps=60
#video-sync=display-resample
#interpolation=yes
#tscale=oversample # smoothmotion


################
# Anti-Ringing #
################

#scale-antiring=0.7 # luma upscale deringing
#dscale-antiring=0.7 # luma downscale deringing
#cscale-antiring=0.7 # chroma upscale deringing


###################################
# Protocol Specific Configuration #
###################################

#[protocol.http]
#hls-bitrate=max # use max quality for HLS streams
#cache=yes
#no-cache-pause # don't pause when the cache runs low

#[protocol.https]
#profile=protocol.http

#[protocol.ytdl]
#profile=protocol.http
