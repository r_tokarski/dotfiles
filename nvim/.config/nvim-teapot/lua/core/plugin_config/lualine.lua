require('lualine').setup {
  options = {
    icons_enabled = true,
   -- theme = 'neosolarized',
  },
  sections = {
    lualine_a = {
      {
        'filename',
        path = 1,
      }
    }
  }
}
