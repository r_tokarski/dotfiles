export QT_QPA_PLATFORMTHEME=xcb
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export EDITOR="/usr/bin/nvim"
export VISUAL="/usr/bin/nvim"
export VAGRANT_DEFAULT_PROVIDER=libvirt
export PATH="$HOME/.local/bin/:$HOME/.cargo/bin/:$PATH"
export BAT_THEME="Dracula"
