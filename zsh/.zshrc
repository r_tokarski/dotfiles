eval "$(starship init zsh)"
eval "$(zoxide init zsh)"

bindkey -e
unsetopt nomatch

autoload -Uz compinit promptinit colors 
compinit
promptinit
colors
plugins=(git)

setopt APPEND_HISTORY
HISTFILE=~/.zhistfile
SAVEHIST=10000

# common aliases
alias grep='grep --color=auto'
alias ll='lsd -lh'
alias llt='lsd -lth'
alias ls='lsd'
alias less='less -R'
alias mkdir='mkdir -p'
alias rsync='rsync --progress -ahz'
alias stow='stow -t ~/'
alias vim='nvim'
alias soz='source ~/.zshrc && source ~/.zshenv'
alias lf='yazi'
alias cat='bat'
alias icat='kitten icat' # kitty required

alias du='du -h'
alias df='df -h'

alias fastfetch='fastfetch --config ~/.config/fastfetch/config.jsonc'

alias mhz='watch -n1 "cat /proc/cpuinfo | grep MHz"'
function datebackup() { cp "$1" "$1.$(date +%FT%H:%M:%S)"; }
alias dcd='cd ../..'

# host-specific aliases
if [[ $(hostnamectl hostname | cut -f1 -d.) == castleyankee ]]; then
  source ~/.zsh.d/castle
elif [[ $(hostnamectl hostname) == teapotwasp ]]; then
  source ~/.zsh.d/teapot
fi

# distro-specific aliases
if [[ $(sed -n '4p' /etc/os-release | cut -f2 -d=) == fedora ]]; then
	source ~/.zsh.d/fedora
elif [[ $(sed -n '3p' /etc/os-release | cut -f2 -d=) =~ opensuse-tumbleweed ]]; then
	source ~/.zsh.d/opensuse
fi

source ~/.zsh.d/secretenv

export PATH=$PATH:/home/tockar/.spicetify
